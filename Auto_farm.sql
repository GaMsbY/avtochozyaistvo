-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 26 2018 г., 06:16
-- Версия сервера: 5.6.38
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `Auto_farm`
--
CREATE DATABASE IF NOT EXISTS `Auto_farm` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `Auto_farm`;

-- --------------------------------------------------------

--
-- Структура таблицы `accesses`
--

CREATE TABLE `accesses` (
  `id_access` int(11) NOT NULL,
  `forms` varchar(64) NOT NULL,
  `abilities` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `accesses`
--

INSERT INTO `accesses` (`id_access`, `forms`, `abilities`) VALUES
(1, '(0,1,2,3,4,5,6,7,8,9,10,11)', 'write'),
(2, '(3,4,7,8,9)', 'read');

-- --------------------------------------------------------

--
-- Структура таблицы `autos`
--

CREATE TABLE `autos` (
  `id_auto` int(11) NOT NULL,
  `mark` varchar(30) NOT NULL,
  `number_g` varchar(9) NOT NULL,
  `id_cargo` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `carrying_capacity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `autos`
--

INSERT INTO `autos` (`id_auto`, `mark`, `number_g`, `id_cargo`, `id_state`, `carrying_capacity`) VALUES
(5, 'Газель', 'А777АА154', 4, 3, 10),
(6, 'Газель', 'Б777ББ154', 5, 3, 10),
(7, 'Газель', 'В777ВВ154', 6, 3, 10),
(8, 'ЗИЛ', 'Г777ГГ154', 7, 3, 15),
(9, 'ЗИЛ', 'Д777ДД154', 8, 3, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `cargos`
--

CREATE TABLE `cargos` (
  `id_cargo` int(11) NOT NULL,
  `cargo` varchar(15) NOT NULL,
  `unit_cost` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cargos`
--

INSERT INTO `cargos` (`id_cargo`, `cargo`, `unit_cost`) VALUES
(4, 'Цемент', 100),
(5, 'Кирпич', 400),
(6, 'Глина', 200),
(7, 'Песок', 100),
(8, 'Щебень', 200);

-- --------------------------------------------------------

--
-- Структура таблицы `classes`
--

CREATE TABLE `classes` (
  `id_class` int(11) NOT NULL,
  `class_d` varchar(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `classes`
--

INSERT INTO `classes` (`id_class`, `class_d`) VALUES
(3, '1 класс'),
(4, '2 класс');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `id_client` int(11) NOT NULL,
  `surname` varchar(15) NOT NULL,
  `address` varchar(30) NOT NULL,
  `telephon` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `drivers`
--

CREATE TABLE `drivers` (
  `id_driver` int(11) NOT NULL,
  `surname` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `patronymic` varchar(15) NOT NULL,
  `id_class` int(11) NOT NULL,
  `experience` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `drivers`
--

INSERT INTO `drivers` (`id_driver`, `surname`, `name`, `patronymic`, `id_class`, `experience`) VALUES
(4, 'Прошин', 'Иван', 'Николаевич', 3, 10),
(5, 'Романович', 'Николай', 'Витальевич', 4, 15),
(6, 'Иванов', 'Иван', 'Иванович', 3, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `driver_auto`
--

CREATE TABLE `driver_auto` (
  `id_entry` int(11) NOT NULL,
  `id_driver` int(11) NOT NULL,
  `id_auto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `driver_auto`
--

INSERT INTO `driver_auto` (`id_entry`, `id_driver`, `id_auto`) VALUES
(6, 4, 5),
(7, 4, 6),
(8, 4, 7),
(9, 4, 8),
(10, 4, 9),
(11, 5, 5),
(12, 5, 6),
(13, 5, 7),
(14, 5, 8),
(15, 5, 9),
(16, 6, 7),
(17, 6, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id_associate` int(11) NOT NULL,
  `surname` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `patronymic` varchar(15) NOT NULL,
  `address` varchar(30) NOT NULL,
  `date_birth` date NOT NULL,
  `id_position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id_associate`, `surname`, `name`, `patronymic`, `address`, `date_birth`, `id_position`) VALUES
(6, 'Енин', 'Андрей', 'Витальевич', 'Немировича-Данченко 85', '1975-10-25', 4),
(7, 'Еремин', 'Роман', 'Ардреевич', 'Тролейная 28', '1995-01-30', 6),
(8, 'Варин', 'Михаил', 'Валериевич', 'Ленина 1', '1985-03-05', 7),
(9, 'Прошин', 'Иван', 'Николаевич', 'Блюхера 11', '1977-07-15', 5),
(10, 'Романович', 'Николай', 'Витальевич', 'Зорге 11', '1977-08-15', 5),
(11, 'Иванов', 'Иван', 'Иванович', 'Московская 56', '1970-05-13', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `entrys`
--

CREATE TABLE `entrys` (
  `id_entry` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_auto` int(11) NOT NULL,
  `id_driver` int(11) NOT NULL,
  `date_order` date NOT NULL,
  `id_cargo` int(11) NOT NULL,
  `start_address` varchar(30) NOT NULL,
  `end_address` varchar(30) NOT NULL,
  `date_work` datetime NOT NULL,
  `weight` int(11) NOT NULL,
  `state_e` varchar(15) NOT NULL,
  `cost_en` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `information_displacement`
--

CREATE TABLE `information_displacement` (
  `id_entry` int(11) NOT NULL,
  `id_associate` int(11) NOT NULL,
  `id_position` int(11) NOT NULL,
  `date_order` date NOT NULL,
  `couse_displacement` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `positions`
--

CREATE TABLE `positions` (
  `id_position` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `position` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `positions`
--

INSERT INTO `positions` (`id_position`, `salary`, `position`) VALUES
(4, 50000, 'Управляющий'),
(5, 40000, 'Водитель'),
(6, 15000, 'Уборщик'),
(7, 35000, 'Бухгалтер');

-- --------------------------------------------------------

--
-- Структура таблицы `states`
--

CREATE TABLE `states` (
  `id_state` int(11) NOT NULL,
  `state_a` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `states`
--

INSERT INTO `states` (`id_state`, `state_a`) VALUES
(3, 'Исправен'),
(4, 'Сломан');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `login` varchar(15) NOT NULL,
  `password` varchar(30) NOT NULL,
  `id_acccess` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id_user`, `login`, `password`, `id_acccess`) VALUES
(2, 'slava', '1925294409', 2),
(3, 'admin', '1156371652', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `accesses`
--
ALTER TABLE `accesses`
  ADD PRIMARY KEY (`id_access`);

--
-- Индексы таблицы `autos`
--
ALTER TABLE `autos`
  ADD PRIMARY KEY (`id_auto`),
  ADD KEY `id_cargo` (`id_cargo`,`id_state`),
  ADD KEY `id_state` (`id_state`);

--
-- Индексы таблицы `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id_cargo`);

--
-- Индексы таблицы `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id_client`);

--
-- Индексы таблицы `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id_driver`),
  ADD KEY `id_class` (`id_class`);

--
-- Индексы таблицы `driver_auto`
--
ALTER TABLE `driver_auto`
  ADD PRIMARY KEY (`id_entry`),
  ADD KEY `id_driver` (`id_driver`,`id_auto`),
  ADD KEY `id_auto` (`id_auto`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id_associate`),
  ADD KEY `id_position` (`id_position`);

--
-- Индексы таблицы `entrys`
--
ALTER TABLE `entrys`
  ADD PRIMARY KEY (`id_entry`),
  ADD KEY `id_client` (`id_client`,`id_auto`,`id_driver`,`id_cargo`),
  ADD KEY `id_auto` (`id_auto`),
  ADD KEY `id_driver` (`id_driver`),
  ADD KEY `id_cargo` (`id_cargo`);

--
-- Индексы таблицы `information_displacement`
--
ALTER TABLE `information_displacement`
  ADD PRIMARY KEY (`id_entry`),
  ADD KEY `id_associate` (`id_associate`,`id_position`),
  ADD KEY `id_position` (`id_position`);

--
-- Индексы таблицы `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id_position`);

--
-- Индексы таблицы `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id_state`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_acccess` (`id_acccess`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `accesses`
--
ALTER TABLE `accesses`
  MODIFY `id_access` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `autos`
--
ALTER TABLE `autos`
  MODIFY `id_auto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id_cargo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `classes`
--
ALTER TABLE `classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id_driver` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `driver_auto`
--
ALTER TABLE `driver_auto`
  MODIFY `id_entry` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id_associate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `entrys`
--
ALTER TABLE `entrys`
  MODIFY `id_entry` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `information_displacement`
--
ALTER TABLE `information_displacement`
  MODIFY `id_entry` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `positions`
--
ALTER TABLE `positions`
  MODIFY `id_position` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `states`
--
ALTER TABLE `states`
  MODIFY `id_state` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `autos`
--
ALTER TABLE `autos`
  ADD CONSTRAINT `autos_ibfk_1` FOREIGN KEY (`id_state`) REFERENCES `states` (`id_state`) ON UPDATE CASCADE,
  ADD CONSTRAINT `autos_ibfk_2` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id_cargo`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `drivers`
--
ALTER TABLE `drivers`
  ADD CONSTRAINT `drivers_ibfk_1` FOREIGN KEY (`id_class`) REFERENCES `classes` (`id_class`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `driver_auto`
--
ALTER TABLE `driver_auto`
  ADD CONSTRAINT `driver_auto_ibfk_1` FOREIGN KEY (`id_driver`) REFERENCES `drivers` (`id_driver`) ON UPDATE CASCADE,
  ADD CONSTRAINT `driver_auto_ibfk_2` FOREIGN KEY (`id_auto`) REFERENCES `autos` (`id_auto`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`id_position`) REFERENCES `positions` (`id_position`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `entrys`
--
ALTER TABLE `entrys`
  ADD CONSTRAINT `entrys_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id_client`) ON UPDATE CASCADE,
  ADD CONSTRAINT `entrys_ibfk_2` FOREIGN KEY (`id_auto`) REFERENCES `autos` (`id_auto`) ON UPDATE CASCADE,
  ADD CONSTRAINT `entrys_ibfk_3` FOREIGN KEY (`id_driver`) REFERENCES `drivers` (`id_driver`) ON UPDATE CASCADE,
  ADD CONSTRAINT `entrys_ibfk_4` FOREIGN KEY (`id_cargo`) REFERENCES `cargos` (`id_cargo`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `information_displacement`
--
ALTER TABLE `information_displacement`
  ADD CONSTRAINT `information_displacement_ibfk_1` FOREIGN KEY (`id_associate`) REFERENCES `employees` (`id_associate`) ON UPDATE CASCADE,
  ADD CONSTRAINT `information_displacement_ibfk_2` FOREIGN KEY (`id_position`) REFERENCES `positions` (`id_position`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_acccess`) REFERENCES `accesses` (`id_access`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class autos : Form
    {
        MySqlConnection connection;
        ShowTables show;
        CheckEnter checkEnter = new CheckEnter();
        bool mark_ch = false;
        bool number_ch = false;
        bool cargo_ch = false;
        bool state_ch = false;
        bool capacity_ch = false;
        string ability = null;
        //Запрос для вывода таблицы
        string show_request = "SELECT au.mark AS Марка, " +
            "au.number_g AS ГосНомер, c.cargo AS Груз, st.state_a AS Состояние, " +
            "au.carrying_capacity AS Грузоподъемность_ц " +
            "FROM autos au " +
            "INNER JOIN states st ON au.id_state = st.id_state " +
            "INNER JOIN cargos c ON au.id_cargo=c.id_cargo;";
        public autos(MySqlConnection connection, string ability)
        {
            string request = null;

            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;
            table_GV.ReadOnly = true;

            if (ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                auto_gb.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "autos");
            show.take_data(show_request);

            request = "SELECT state_a FROM states";
            show.get_selector(request, state_tb);
            connection.Close();

            request = "SELECT cargo FROM cargos";
            connection.Open();
            show.get_selector(request, cargo_tb);
            connection.Close();

            mark_tb.TextChanged += Mark_tb_TextChanged;
            number_g_tb.TextChanged += Number_g_tb_TextChanged;
            cargo_tb.TextChanged += Cargo_tb_TextChanged;
            state_tb.TextChanged += State_tb_TextChanged;
            capacity_tb.TextChanged += Capacity_tb_TextChanged;
        }

        //проверки ввода
        private void Capacity_tb_TextChanged(object sender, EventArgs e)
        {
            capacity_ch = checkEnter.checkFormatTextBox(@"^\d+$", capacity_tb.Text, capacity_tb, this.auto_gb);
        }

        private void State_tb_TextChanged(object sender, EventArgs e)
        {
            state_ch = checkEnter.checkFormatComboBox(null, state_tb.Text, state_tb, this.auto_gb);
        }

        private void Cargo_tb_TextChanged(object sender, EventArgs e)
        {
            cargo_ch = checkEnter.checkFormatComboBox(null, cargo_tb.Text, cargo_tb, this.auto_gb);
        }

        private void Number_g_tb_TextChanged(object sender, EventArgs e)
        {
            number_ch = checkEnter.checkFormatTextBox(@"^[А-Я\d]+$", number_g_tb.Text, number_g_tb, this.auto_gb);
        }

        private void Mark_tb_TextChanged(object sender, EventArgs e)
        {
            mark_ch = checkEnter.checkFormatTextBox(null, mark_tb.Text, mark_tb, this.auto_gb);
        }
        //----------------------------------------
        private void add_btn_Click(object sender, EventArgs e)
        {
            number_ch = checkEnter.checkFormatTextBox(@"^[А-Я]\d{3,3}[А-Я][А-Я]\d{2,3}$", number_g_tb.Text, number_g_tb, auto_gb);

            if (mark_ch && number_ch && cargo_ch && state_ch && capacity_ch)
            {
                string request = null;

                string mark = mark_tb.Text;
                string number_g = number_g_tb.Text;
                string cargo = cargo_tb.Text;
                string state = state_tb.Text;
                int capacity;
                int.TryParse(capacity_tb.Text, out capacity);

                int id_cargo = 0;
                int id_state = 0;

                request = "SELECT id_cargo FROM cargos WHERE cargo =\"" + cargo + "\"";
                connection.Open();
                id_cargo = show.get_id(request);
                connection.Close();

                request = "SELECT id_state FROM states WHERE state_a =\"" + state + "\"";
                connection.Open();
                id_state = show.get_id(request);
                connection.Close();

                request = "INSERT INTO autos(mark, number_g, id_cargo, id_state, carrying_capacity) " +
                    "VALUES(\"" + mark + "\",\"" + number_g + "\"," + id_cargo + "," + id_state + "," + capacity + ");";
                connection.Open();
                show.make_request(request);
                connection.Close();
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            string request = null;

            DataGridViewRow row = table_GV.SelectedRows[0];
            string number_g = (string)row.Cells[2].Value;

            request = "DELETE FROM autos WHERE number_g= \"" + number_g + "\";";
            connection.Open();
            show.make_request(request);
            connection.Close();
        }

        private void autos_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}

﻿namespace Avtohozyaistvo
{
    partial class exitDocument
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.end_address_l = new System.Windows.Forms.Label();
            this.date_time_l = new System.Windows.Forms.Label();
            this.unit_cost_l = new System.Windows.Forms.Label();
            this.cost_l = new System.Windows.Forms.Label();
            this.date_order_l = new System.Windows.Forms.Label();
            this.cargo_l = new System.Windows.Forms.Label();
            this.start_address_l = new System.Windows.Forms.Label();
            this.address_l = new System.Windows.Forms.Label();
            this.telephon_l = new System.Windows.Forms.Label();
            this.auto_l = new System.Windows.Forms.Label();
            this.fio_l = new System.Windows.Forms.Label();
            this.surname_l = new System.Windows.Forms.Label();
            this.agree_btn = new System.Windows.Forms.Button();
            this.disagree_btn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия клиета:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(64, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "ФИО водителя:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Автомобиль:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Телефон клиента:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(66, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Адрес клиента:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(71, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Адрес подачи:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(118, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Груз:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(76, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Дата заказа:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(86, 287);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 13);
            this.label9.TabIndex = 9;
            this.label9.Text = "Стоимость:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(56, 258);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(95, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Цена за еденицу:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 232);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(145, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Дата и время выполнения:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(48, 205);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Адрес назначения:";
            // 
            // end_address_l
            // 
            this.end_address_l.AutoSize = true;
            this.end_address_l.Location = new System.Drawing.Point(157, 205);
            this.end_address_l.Name = "end_address_l";
            this.end_address_l.Size = new System.Drawing.Size(103, 13);
            this.end_address_l.TabIndex = 24;
            this.end_address_l.Text = "Адрес назначения:";
            // 
            // date_time_l
            // 
            this.date_time_l.AutoSize = true;
            this.date_time_l.Location = new System.Drawing.Point(157, 232);
            this.date_time_l.Name = "date_time_l";
            this.date_time_l.Size = new System.Drawing.Size(145, 13);
            this.date_time_l.TabIndex = 23;
            this.date_time_l.Text = "Дата и время выполнения:";
            // 
            // unit_cost_l
            // 
            this.unit_cost_l.AutoSize = true;
            this.unit_cost_l.Location = new System.Drawing.Point(157, 258);
            this.unit_cost_l.Name = "unit_cost_l";
            this.unit_cost_l.Size = new System.Drawing.Size(95, 13);
            this.unit_cost_l.TabIndex = 22;
            this.unit_cost_l.Text = "Цена за еденицу:";
            // 
            // cost_l
            // 
            this.cost_l.AutoSize = true;
            this.cost_l.Location = new System.Drawing.Point(157, 287);
            this.cost_l.Name = "cost_l";
            this.cost_l.Size = new System.Drawing.Size(65, 13);
            this.cost_l.TabIndex = 21;
            this.cost_l.Text = "Стоимость:";
            // 
            // date_order_l
            // 
            this.date_order_l.AutoSize = true;
            this.date_order_l.Location = new System.Drawing.Point(157, 128);
            this.date_order_l.Name = "date_order_l";
            this.date_order_l.Size = new System.Drawing.Size(75, 13);
            this.date_order_l.TabIndex = 20;
            this.date_order_l.Text = "Дата заказа:";
            // 
            // cargo_l
            // 
            this.cargo_l.AutoSize = true;
            this.cargo_l.Location = new System.Drawing.Point(157, 151);
            this.cargo_l.Name = "cargo_l";
            this.cargo_l.Size = new System.Drawing.Size(33, 13);
            this.cargo_l.TabIndex = 19;
            this.cargo_l.Text = "Груз:";
            // 
            // start_address_l
            // 
            this.start_address_l.AutoSize = true;
            this.start_address_l.Location = new System.Drawing.Point(157, 176);
            this.start_address_l.Name = "start_address_l";
            this.start_address_l.Size = new System.Drawing.Size(79, 13);
            this.start_address_l.TabIndex = 18;
            this.start_address_l.Text = "Адрес подачи:";
            // 
            // address_l
            // 
            this.address_l.AutoSize = true;
            this.address_l.Location = new System.Drawing.Point(157, 34);
            this.address_l.Name = "address_l";
            this.address_l.Size = new System.Drawing.Size(85, 13);
            this.address_l.TabIndex = 17;
            this.address_l.Text = "Адрес клиента:";
            // 
            // telephon_l
            // 
            this.telephon_l.AutoSize = true;
            this.telephon_l.Location = new System.Drawing.Point(157, 56);
            this.telephon_l.Name = "telephon_l";
            this.telephon_l.Size = new System.Drawing.Size(99, 13);
            this.telephon_l.TabIndex = 16;
            this.telephon_l.Text = "Телефон клиента:";
            // 
            // auto_l
            // 
            this.auto_l.AutoSize = true;
            this.auto_l.Location = new System.Drawing.Point(157, 81);
            this.auto_l.Name = "auto_l";
            this.auto_l.Size = new System.Drawing.Size(72, 13);
            this.auto_l.TabIndex = 15;
            this.auto_l.Text = "Автомобиль:";
            // 
            // fio_l
            // 
            this.fio_l.AutoSize = true;
            this.fio_l.Location = new System.Drawing.Point(157, 104);
            this.fio_l.Name = "fio_l";
            this.fio_l.Size = new System.Drawing.Size(87, 13);
            this.fio_l.TabIndex = 14;
            this.fio_l.Text = "ФИО водителя:";
            // 
            // surname_l
            // 
            this.surname_l.AutoSize = true;
            this.surname_l.Location = new System.Drawing.Point(157, 9);
            this.surname_l.Name = "surname_l";
            this.surname_l.Size = new System.Drawing.Size(97, 13);
            this.surname_l.TabIndex = 13;
            this.surname_l.Text = "Фамилия клиета:";
            // 
            // agree_btn
            // 
            this.agree_btn.Location = new System.Drawing.Point(13, 309);
            this.agree_btn.Name = "agree_btn";
            this.agree_btn.Size = new System.Drawing.Size(75, 23);
            this.agree_btn.TabIndex = 25;
            this.agree_btn.Text = "Оформить";
            this.agree_btn.UseVisualStyleBackColor = true;
            this.agree_btn.Click += new System.EventHandler(this.agree_btn_Click);
            // 
            // disagree_btn
            // 
            this.disagree_btn.Location = new System.Drawing.Point(231, 309);
            this.disagree_btn.Name = "disagree_btn";
            this.disagree_btn.Size = new System.Drawing.Size(75, 23);
            this.disagree_btn.TabIndex = 26;
            this.disagree_btn.Text = "Отказаться";
            this.disagree_btn.UseVisualStyleBackColor = true;
            this.disagree_btn.Click += new System.EventHandler(this.disagree_btn_Click);
            // 
            // exitDocument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 344);
            this.Controls.Add(this.disagree_btn);
            this.Controls.Add(this.agree_btn);
            this.Controls.Add(this.end_address_l);
            this.Controls.Add(this.date_time_l);
            this.Controls.Add(this.unit_cost_l);
            this.Controls.Add(this.cost_l);
            this.Controls.Add(this.date_order_l);
            this.Controls.Add(this.cargo_l);
            this.Controls.Add(this.start_address_l);
            this.Controls.Add(this.address_l);
            this.Controls.Add(this.telephon_l);
            this.Controls.Add(this.auto_l);
            this.Controls.Add(this.fio_l);
            this.Controls.Add(this.surname_l);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "exitDocument";
            this.Text = "Заказ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label end_address_l;
        private System.Windows.Forms.Label date_time_l;
        private System.Windows.Forms.Label unit_cost_l;
        private System.Windows.Forms.Label cost_l;
        private System.Windows.Forms.Label date_order_l;
        private System.Windows.Forms.Label cargo_l;
        private System.Windows.Forms.Label start_address_l;
        private System.Windows.Forms.Label address_l;
        private System.Windows.Forms.Label telephon_l;
        private System.Windows.Forms.Label auto_l;
        private System.Windows.Forms.Label fio_l;
        private System.Windows.Forms.Label surname_l;
        private System.Windows.Forms.Button agree_btn;
        private System.Windows.Forms.Button disagree_btn;
    }
}
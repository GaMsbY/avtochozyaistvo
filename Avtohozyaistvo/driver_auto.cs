﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class driver_auto : Form
    {
        MySqlConnection connection;
        ShowTables show;
        CheckEnter checkEnter = new CheckEnter();
        bool driver_ch = false;
        bool auto_ch = false;
        string ability = null;
        string show_request = "SELECT dr.surname AS Фамилия, dr.name AS Имя, dr.patronymic AS Отчество, " +
            "au.mark AS Марка, au.number_g AS ГосНомер " +
            "FROM driver_auto da " +
            "INNER JOIN drivers dr ON dr.id_driver = da.id_driver " +
            "INNER JOIN autos au ON au.id_auto=da.id_auto";
        public driver_auto(MySqlConnection connection, string ability)
        {
            string request = null;

            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;
            table_GV.ReadOnly = true;

            if (ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                driver_auto_gb.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "drivers");
            show.take_data(show_request);

            request = "SELECT surname, name, patronymic FROM drivers";
            show.get_selector(request, driver_tb);
            connection.Close();

            request = "SELECT mark, number_g FROM autos";
            connection.Open();
            show.get_selector(request, auto_tb);
            connection.Close();

            driver_tb.TextChanged += Driver_tb_TextChanged;
            auto_tb.TextChanged += Auto_tb_TextChanged;
        }

        private void Auto_tb_TextChanged(object sender, EventArgs e)
        {
            auto_ch = checkEnter.checkFormatComboBox(null, auto_tb.Text, auto_tb, this.driver_auto_gb);
        }

        private void Driver_tb_TextChanged(object sender, EventArgs e)
        {
            driver_ch = checkEnter.checkFormatComboBox(null, driver_tb.Text, driver_tb, this.driver_auto_gb);
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (driver_ch && auto_ch)
            {
                string request = null;

                string[] driver = driver_tb.Text.Split(' ');
                string[] auto = auto_tb.Text.Split(' ');

                request = "SELECT id_driver FROM drivers " +
                    "WHERE surname =\"" + driver[0] + "\" " +
                    "AND name=\"" + driver[1] + "\" " +
                    "AND patronymic=\"" + driver[2] + "\"";
                connection.Open();
                int id_driver = show.get_id(request);
                connection.Close();

                request = "SELECT id_auto FROM autos WHERE mark =\"" + auto[0] + "\" AND number_g=\"" + auto[1] + "\"";
                connection.Open();
                int id_auto = show.get_id(request);
                connection.Close();

                request = "INSERT INTO driver_auto(id_driver, id_auto) VALUES(" + id_driver + ", " + id_auto + ");";
                connection.Open();
                show.make_request(request);
                connection.Close();
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            string request = null;

            DataGridViewRow row = table_GV.SelectedRows[0];
            string surname = (string)row.Cells[0].Value;
            string name = (string)row.Cells[1].Value;
            string patronymic = (string)row.Cells[2].Value;
            string mark = (string)row.Cells[3].Value;
            string number_g = (string)row.Cells[4].Value;

            request = "SELECT id_driver FROM drivers " +
                "WHERE surname =\"" + surname + "\" AND name=\"" + name + "\" AND patronymic=\"" + patronymic + "\"";
            connection.Open();
            int id_driver = show.get_id(request);
            connection.Close();

            request = "SELECT id_auto FROM autos WHERE mark =\"" + mark + "\" AND number_g=\"" + number_g + "\"";
            connection.Open();
            int id_auto = show.get_id(request);
            connection.Close();

            request = "DELETE FROM driver_auto WHERE id_driver=\"" + id_driver + "\" AND id_auto=\"" + id_auto + "\"";
            connection.Open();
            show.make_request(request);
            connection.Close();
        }

        private void driver_auto_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Avtohozyaistvo
{
    public class ShowTables
    {
        MySqlConnection connection;
        DataSet ds;
        MySqlDataAdapter adapter;
        DataGridView table_GV;
        string name_table;
        public string request;
        public ShowTables(MySqlConnection connection, DataGridView table_GV, string name_table)//конструктор при работе с таблицей
        {
            this.connection = connection;
            this.table_GV = table_GV;
            this.name_table = name_table;
            this.table_GV.CellValidating += Table_GV_CellValidating;
            this.table_GV.CellEndEdit += Table_GV_CellEndEdit;
            
        }
        public ShowTables(MySqlConnection connection, string name_table)//конструктор при работе без таблицы
        {
            this.connection = connection;
            this.name_table = name_table;
        }
        private void Table_GV_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            switch (name_table)
            {
                case "positions":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        if (e.ColumnIndex == 2)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        break;
                    }
                case "states":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        break;
                    }
                case "clients":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        if (e.ColumnIndex == 2)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        if (e.ColumnIndex == 3)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        break;
                    }
                case "classes":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        break;
                    }
                case "cargos":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        if (e.ColumnIndex == 2)
                        {
                            table_GV.Rows[e.RowIndex].ErrorText = String.Empty;
                        }
                        break;
                    }
            }
        }
        private void Table_GV_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            switch (name_table)
            {
                case "positions":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            string pattern = @"^\d+$";
                            if((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Оплата должна содержать только цифры.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        if (e.ColumnIndex == 2)
                        {
                            string pattern = @"^[а-яА-Я]+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Должность должна содержать только русские буквы.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        break;
                    }
                case "states":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            string pattern = @"^[а-яА-Я]+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Состояние должно содержать только русские буквы.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        break;
                    }
                case "clients":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            string pattern = @"^[а-яА-Я]+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Име должно содержать только русские буквы.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        if (e.ColumnIndex == 2)
                        {
                            string pattern = @"^[а-яА-Я \d]+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Адрес может содержать только русские буквы, цифры и пробелы.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        if (e.ColumnIndex == 3)
                        {
                            string pattern = @"^8-\d{3}-\d{3}-\d{2}-\d{2}$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Задайте номер телефона в формате 8-XXX-XXX-XX-XX.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        break;
                    }
                case "classes":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            string pattern = @"^[а-яА-Я \d]+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Состояние должно содержать только русские буквы.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        break;
                    }
                case "cargos":
                    {
                        if (e.ColumnIndex == 1)
                        {
                            string pattern = @"^[а-яА-Я ]+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Груз должнен содержать только буквы и пробелы.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        if (e.ColumnIndex == 2)
                        {
                            string pattern = @"^\d+$";
                            if ((string)e.FormattedValue == string.Empty)
                            {
                                break;
                            }
                            if (!(Regex.IsMatch((string)e.FormattedValue, pattern)))
                            {
                                table_GV.Rows[e.RowIndex].ErrorText = "Цена за еденицу может содержать только цифры.\n" +
                                    "Для завершения редактирования нажмите ESC";
                                e.Cancel = true;
                            }
                        }
                        break;
                    }
            }
        }
        public void take_data(string request)
        {
            this.request = request;
            adapter = new MySqlDataAdapter(request, connection);
            ds = new DataSet();
            adapter.Fill(ds);
            table_GV.DataSource = ds.Tables[0];
            table_GV.Columns[0].Visible = false;
        }
        public void add_data_auto()
        {
            DataRow row = ds.Tables[0].NewRow();
            ds.Tables[0].Rows.Add(row);
        }
        public void delete_data_auto()
        {
            foreach (DataGridViewRow row in table_GV.SelectedRows)
            {
                table_GV.Rows.Remove(row);
            }
        }
        public void save_data()
        {
            MySqlCommandBuilder commandBuilder = new MySqlCommandBuilder(adapter);
            adapter.Update(ds.Tables[0]);
            ds.Clear();
            adapter.Fill(ds);
        }
        public void update()
        {
            ds.Clear();
            adapter.Fill(ds);
        }
        public void make_request(string add_request)
        {
            MySqlCommand command = new MySqlCommand(add_request, connection);
            command.ExecuteNonQuery();
            this.update();
        }
        public void make_request_unupdate(string add_request)
        {
            MySqlCommand command = new MySqlCommand(add_request, connection);
            command.ExecuteNonQuery();
        }
        public int get_id(string request)
        {
            int id_position;
            MySqlCommand command = new MySqlCommand(request, connection);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    id_position = (int)reader.GetValue(0);
                    return id_position;
                }
            }
            return -1;
        }
        public void get_selector(string request, ComboBox comboBox)
        {
            bool first = true;
            MySqlCommand command = new MySqlCommand(request, connection);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows) // если есть данные
            {
                int fieldcount = reader.FieldCount;
                string str_for_select = null;
                while (reader.Read()) // построчно считываем данные
                {
                    str_for_select = null;
                    for (int i = 0; i < fieldcount; i++)
                    {
                        if (i != 0) str_for_select += " ";
                        str_for_select += (string)reader.GetValue(i);
                    }
                    //if(first)
                    //{
                    //    first = false;
                    //    comboBox.Text = str_for_select;
                    //}
                    comboBox.Items.Add(str_for_select);
                }
            }
        }
        public void update_database()
        {
            DateTime now_date = DateTime.Now.AddHours(-1);
            List<int> id_entrys = new List<int>();
            string select_request = "SELECT id_entry FROM entrys WHERE date_work <\"" + now_date.ToString("yyyy-MM-dd HH:mm:ss") + "\"";
            MySqlCommand command = new MySqlCommand(select_request, connection);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows) // если есть данные
            {
                while (reader.Read()) // построчно считываем данные
                {
                    id_entrys.Add((int)reader.GetValue(0));
                }
            }
            connection.Close();
            connection.Open();
            for(int i = 0; i < id_entrys.Count; i++)
            {
                string update_request = "UPDATE entrys SET state_e=\"Выполнен\" WHERE id_entry=" + id_entrys[i];
                make_request_unupdate(update_request);
            }
            id_entrys.Clear();
            connection.Close();

            now_date = DateTime.Now.AddYears(-1);
            select_request = "SELECT id_entry FROM entrys WHERE date_order <\"" + now_date.ToString("yyyy-MM-dd") + "\"";
            connection.Open();
            command = new MySqlCommand(select_request, connection);
            reader = command.ExecuteReader();
            if (reader.HasRows) // если есть данные
            {
                while (reader.Read()) // построчно считываем данные
                {
                    id_entrys.Add((int)reader.GetValue(0));
                }
            }
            connection.Close();
            connection.Open();
            for (int i = 0; i < id_entrys.Count; i++)
            {
                string update_request = "DELETE FROM entrys WHERE id_entry=" + id_entrys[i];
                make_request_unupdate(update_request);
            }
            id_entrys.Clear();
            connection.Close();


            now_date = DateTime.Now.AddYears(-5);
            select_request = "SELECT id_entry FROM information_displacement WHERE date_order <\"" + now_date.ToString("yyyy-MM-dd") + "\"";
            connection.Open();
            command = new MySqlCommand(select_request, connection);
            reader = command.ExecuteReader();
            if (reader.HasRows) // если есть данные
            {
                while (reader.Read()) // построчно считываем данные
                {
                    id_entrys.Add((int)reader.GetValue(0));
                }
            }
            connection.Close();
            connection.Open();
            for (int i = 0; i < id_entrys.Count; i++)
            {
                string update_request = "DELETE FROM information_displacement WHERE id_entry=" + id_entrys[i];
                make_request_unupdate(update_request);
            }
        }
    }
}

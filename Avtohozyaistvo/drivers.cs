﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class drivers : Form
    {
        MySqlConnection connection;
        ShowTables show;
        CheckEnter checkEnter = new CheckEnter();
        bool driver_ch = false;
        bool class_ch = false;
        bool experience_ch = false;
        string ability = null;
        string show_request = "SELECT dr.surname AS Фамилия, dr.name AS Имя, dr.patronymic AS Отчество, " +
            "cl.class_d AS Классность, dr.experience AS Стаж_лет " +
            "FROM drivers dr " +
            "INNER JOIN classes cl ON cl.id_class = dr.id_class";
        public drivers(MySqlConnection connection, string ability)
        {
            string request = null;

            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;
            table_GV.ReadOnly = true;

            if (ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                driver_gb.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "drivers");
            show.take_data(show_request);

            request = "SELECT class_d FROM classes";
            show.get_selector(request, class_tb);
            connection.Close();

            request = "SELECT em.surname, em.name, em.patronymic " +
                "FROM employees em " +
                "INNER JOIN positions pos ON pos.id_position = em.id_position " +
                "WHERE position=\"Водитель\"";
            connection.Open();
            show.get_selector(request, driver_tb);
            connection.Close();

            driver_tb.TextChanged += Driver_tb_TextChanged;
            class_tb.TextChanged += Class_tb_TextChanged;
            experience_tb.TextChanged += Experience_tb_TextChanged;
        }

        private void Experience_tb_TextChanged(object sender, EventArgs e)
        {
            experience_ch = checkEnter.checkFormatTextBox(null, experience_tb.Text, experience_tb, this.driver_gb);
        }

        private void Class_tb_TextChanged(object sender, EventArgs e)
        {
            class_ch = checkEnter.checkFormatComboBox(null, class_tb.Text, class_tb, this.driver_gb);
        }

        private void Driver_tb_TextChanged(object sender, EventArgs e)
        {
            driver_ch = checkEnter.checkFormatComboBox(null, driver_tb.Text, driver_tb, this.driver_gb);
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (driver_ch && class_ch && experience_ch)
            {
                string request = null;

                string[] driver = driver_tb.Text.Split(' ');
                string class_d = class_tb.Text;
                string experience = experience_tb.Text;

                int id_class = 0;

                request = "SELECT id_class FROM classes WHERE class_d =\"" + class_d + "\"";
                connection.Open();
                id_class = show.get_id(request);
                connection.Close();

                request = "INSERT INTO drivers(surname, name, patronymic, id_class, experience) " +
                    "VALUES(\"" + driver[0] + "\",\"" + driver[1] + "\",\"" + driver[2] + "\"," + id_class + "," + experience + ");";
                connection.Open();
                show.make_request(request);
                connection.Close();
            }
            else
            {
                MessageBox.Show("Введите все данные");
            }
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            string request = null;

            DataGridViewRow row = table_GV.SelectedRows[0];
            string surname = (string)row.Cells[0].Value;
            string name = (string)row.Cells[1].Value;
            string patronymic = (string)row.Cells[2].Value;

            request = "DELETE FROM drivers " +
                "WHERE surname=\"" + surname + "\" " +
                "AND name=\"" + name + "\"" +
                "AND patronymic=\"" + patronymic + "\";";
            connection.Open();
            show.make_request(request);
            connection.Close();
        }

        private void drivers_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    class DBUtils
    {
        public static MySqlConnection GetDBConnection()
        {
            string host = "localhost";
            int port = 3306;
            string database = "Auto_farm";
            string username = "mysql";
            string password = "mysql";

            return DBMySQLUtils.GetDBConnection(host, port, database, username, password);
        }
    }
}

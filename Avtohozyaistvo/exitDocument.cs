﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class exitDocument : Form
    {
        private MySqlConnection connection;
        string request;
        ShowTables show;
        public exitDocument(MySqlConnection connection, string surname, string address, string telephon, int id_auto_add, int id_driver_add, DateTime date_order, int id_cargo, string start_address, string end_address, DateTime dateTime, int weight, int unit_cost)
        {
            MySqlCommand command;
            MySqlDataReader reader;
            this.connection = connection;
            InitializeComponent();
            show = new ShowTables(connection, "makeentry");
            surname_l.Text = surname;
            address_l.Text = address;
            telephon_l.Text = telephon;
            request = "SELECT mark, number_g FROM autos WHERE id_auto=" + id_auto_add;
            connection.Open();
            command = new MySqlCommand(request, connection);
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    auto_l.Text = ((string)reader.GetValue(0)) + " " + ((string)reader.GetValue(1));
                }
            }
            connection.Close();
            request = "SELECT surname, name, patronymic FROM drivers WHERE id_driver=" + id_driver_add;
            connection.Open();
            command = new MySqlCommand(request, connection);
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    fio_l.Text = ((string)reader.GetValue(0)) + ((string)reader.GetValue(1)) + ((string)reader.GetValue(2));
                }
            }
            connection.Close();
            date_order_l.Text = date_order.Date.ToString("yyyy-MM-dd");
            request = "SELECT cargo FROM cargos WHERE id_cargo=" + id_cargo;
            connection.Open();
            command = new MySqlCommand(request, connection);
            reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {

                    cargo_l.Text = ((string)reader.GetValue(0)) + ", " + unit_cost.ToString();
                }
            }
            connection.Close();
            start_address_l.Text = start_address;
            end_address_l.Text = end_address;
            date_time_l.Text = dateTime.ToString("yyyy-MM-dd HH:mm:ss");
            unit_cost_l.Text = weight.ToString();
            cost_l.Text = (weight * unit_cost).ToString();
        }

        private void agree_btn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void disagree_btn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }
    }
}

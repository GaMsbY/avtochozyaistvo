﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class login : Form
    {
        private MySqlConnection connection;
        private ShowTables show;
        private string access = null;
        private string ability = null;
        public login(MySqlConnection connection, ref string access)
        {
            InitializeComponent();
            this.Connection = connection;
            this.Access = access;
            Show1 = new ShowTables(connection, "login");
        }

        public string Access
        {
            get
            {
                return access;
            }
            set
            {
                access = value;
            }
        }
        public MySqlConnection Connection
        {
            get
            {
                return connection;
            }
            set
            {
                connection = value;
            }
        }
        public ShowTables Show1
        {
            get
            {
                return show;
            }
            set
            {
                show = value;
            }
        }
        public string Ability
        {
            get
            {
                return ability;
            }
            set
            {
                ability = value;
            }
        }

        private void enter_btn_Click(object sender, EventArgs e)
        {
            string request = null;
            request = "SELECT ac.forms, ac.abilities " +
                "FROM accesses ac " +
                "INNER JOIN users us ON us.id_acccess=ac.id_access " +
                "WHERE us.login = \"" + login_tb.Text + "\" AND us.password = \"" + password_tb.Text.GetHashCode() + "\"";
            Connection.Open();
            MySqlCommand command = new MySqlCommand(request, Connection);
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Access = (string)reader.GetValue(0);
                    Ability = (string)reader.GetValue(1);
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                    break;
                }
            }
            else
            {
                MessageBox.Show("Такой пользователь отсутствует");
            }
            Connection.Close();
        }

        private void create_tb_Click(object sender, EventArgs e)
        {
            checkin checkIn = new checkin(connection);
            checkIn.Show();
        }
    }
}

﻿namespace Avtohozyaistvo
{
    partial class driver_auto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.driver_tb = new System.Windows.Forms.ComboBox();
            this.auto_tb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.delete_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.table_GV = new System.Windows.Forms.DataGridView();
            this.driver_auto_gb = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).BeginInit();
            this.driver_auto_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // driver_tb
            // 
            this.driver_tb.FormattingEnabled = true;
            this.driver_tb.Location = new System.Drawing.Point(85, 19);
            this.driver_tb.Name = "driver_tb";
            this.driver_tb.Size = new System.Drawing.Size(388, 21);
            this.driver_tb.TabIndex = 68;
            // 
            // auto_tb
            // 
            this.auto_tb.FormattingEnabled = true;
            this.auto_tb.Location = new System.Drawing.Point(85, 45);
            this.auto_tb.Name = "auto_tb";
            this.auto_tb.Size = new System.Drawing.Size(388, 21);
            this.auto_tb.TabIndex = 67;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 48);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 66;
            this.label6.Text = "Автомобиль";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 65;
            this.label1.Text = "Водитель";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(695, 213);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(97, 23);
            this.delete_btn.TabIndex = 64;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(12, 213);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 23);
            this.add_btn.TabIndex = 63;
            this.add_btn.Text = "Добавить в БД";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // table_GV
            // 
            this.table_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table_GV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.table_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_GV.Location = new System.Drawing.Point(12, 12);
            this.table_GV.Name = "table_GV";
            this.table_GV.Size = new System.Drawing.Size(780, 111);
            this.table_GV.TabIndex = 62;
            // 
            // driver_auto_gb
            // 
            this.driver_auto_gb.Controls.Add(this.driver_tb);
            this.driver_auto_gb.Controls.Add(this.label1);
            this.driver_auto_gb.Controls.Add(this.auto_tb);
            this.driver_auto_gb.Controls.Add(this.label6);
            this.driver_auto_gb.Location = new System.Drawing.Point(166, 129);
            this.driver_auto_gb.Name = "driver_auto_gb";
            this.driver_auto_gb.Size = new System.Drawing.Size(479, 78);
            this.driver_auto_gb.TabIndex = 69;
            this.driver_auto_gb.TabStop = false;
            this.driver_auto_gb.Text = "Связь водителя и автомобиля";
            // 
            // driver_auto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 248);
            this.Controls.Add(this.driver_auto_gb);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.table_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "driver_auto";
            this.Text = "Связь водителя и автомобиля";
            this.Load += new System.EventHandler(this.driver_auto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).EndInit();
            this.driver_auto_gb.ResumeLayout(false);
            this.driver_auto_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox driver_tb;
        private System.Windows.Forms.ComboBox auto_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.DataGridView table_GV;
        private System.Windows.Forms.GroupBox driver_auto_gb;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace Avtohozyaistvo
{
    public partial class positions : Form
    {
        MySqlConnection connection;
        ShowTables show;
        string ability = null;
        string request = "SELECT pos.id_position, pos.salary AS Оплата_руб, pos.position AS Должность FROM positions pos";
        public positions(MySqlConnection connection, string ability)
        {
            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            if(ability == "read")
            {
                add_btn.Visible = false;
                save_btn.Visible = false;
                delete_btn.Visible = false;
                table_GV.Height = this.Height - 60;
            }
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false; ;
            connection.Open();
            show = new ShowTables(connection, table_GV, "positions");
            show.take_data(request);
            connection.Close();
        }
        private void save_btn_Click(object sender, EventArgs e)
        {
            show.save_data();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            show.add_data_auto();
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            show.delete_data_auto();
        }

        private void positions_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}

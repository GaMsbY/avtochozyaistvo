﻿namespace Avtohozyaistvo
{
    partial class autos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cargo_tb = new System.Windows.Forms.ComboBox();
            this.state_tb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.delete_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.table_GV = new System.Windows.Forms.DataGridView();
            this.mark_tb = new System.Windows.Forms.TextBox();
            this.number_g_tb = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.capacity_tb = new System.Windows.Forms.TextBox();
            this.auto_gb = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).BeginInit();
            this.auto_gb.SuspendLayout();
            this.SuspendLayout();
            // 
            // cargo_tb
            // 
            this.cargo_tb.FormattingEnabled = true;
            this.cargo_tb.Location = new System.Drawing.Point(124, 71);
            this.cargo_tb.Name = "cargo_tb";
            this.cargo_tb.Size = new System.Drawing.Size(350, 21);
            this.cargo_tb.TabIndex = 53;
            // 
            // state_tb
            // 
            this.state_tb.FormattingEnabled = true;
            this.state_tb.Location = new System.Drawing.Point(124, 97);
            this.state_tb.Name = "state_tb";
            this.state_tb.Size = new System.Drawing.Size(350, 21);
            this.state_tb.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(57, 100);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 13);
            this.label6.TabIndex = 51;
            this.label6.Text = "Состояние";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(88, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 48;
            this.label1.Text = "Груз";
            // 
            // delete_btn
            // 
            this.delete_btn.Location = new System.Drawing.Point(695, 290);
            this.delete_btn.Name = "delete_btn";
            this.delete_btn.Size = new System.Drawing.Size(97, 23);
            this.delete_btn.TabIndex = 47;
            this.delete_btn.Text = "Удалить";
            this.delete_btn.UseVisualStyleBackColor = true;
            this.delete_btn.Click += new System.EventHandler(this.delete_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(12, 290);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(97, 23);
            this.add_btn.TabIndex = 46;
            this.add_btn.Text = "Добавить в БД";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // table_GV
            // 
            this.table_GV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.table_GV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.table_GV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.table_GV.Location = new System.Drawing.Point(12, 12);
            this.table_GV.Name = "table_GV";
            this.table_GV.Size = new System.Drawing.Size(780, 111);
            this.table_GV.TabIndex = 45;
            // 
            // mark_tb
            // 
            this.mark_tb.Location = new System.Drawing.Point(124, 19);
            this.mark_tb.Name = "mark_tb";
            this.mark_tb.Size = new System.Drawing.Size(350, 20);
            this.mark_tb.TabIndex = 56;
            // 
            // number_g_tb
            // 
            this.number_g_tb.Location = new System.Drawing.Point(124, 45);
            this.number_g_tb.Name = "number_g_tb";
            this.number_g_tb.Size = new System.Drawing.Size(350, 20);
            this.number_g_tb.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 58;
            this.label3.Text = "Марка";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(55, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 59;
            this.label5.Text = "Гос. номер";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Грузоподъемность";
            // 
            // capacity_tb
            // 
            this.capacity_tb.Location = new System.Drawing.Point(124, 124);
            this.capacity_tb.Name = "capacity_tb";
            this.capacity_tb.Size = new System.Drawing.Size(350, 20);
            this.capacity_tb.TabIndex = 60;
            // 
            // auto_gb
            // 
            this.auto_gb.Controls.Add(this.mark_tb);
            this.auto_gb.Controls.Add(this.label2);
            this.auto_gb.Controls.Add(this.label1);
            this.auto_gb.Controls.Add(this.capacity_tb);
            this.auto_gb.Controls.Add(this.label6);
            this.auto_gb.Controls.Add(this.label5);
            this.auto_gb.Controls.Add(this.state_tb);
            this.auto_gb.Controls.Add(this.label3);
            this.auto_gb.Controls.Add(this.cargo_tb);
            this.auto_gb.Controls.Add(this.number_g_tb);
            this.auto_gb.Location = new System.Drawing.Point(171, 129);
            this.auto_gb.Name = "auto_gb";
            this.auto_gb.Size = new System.Drawing.Size(480, 155);
            this.auto_gb.TabIndex = 62;
            this.auto_gb.TabStop = false;
            this.auto_gb.Text = "Добавить автомобиль";
            // 
            // autos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 326);
            this.Controls.Add(this.auto_gb);
            this.Controls.Add(this.delete_btn);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.table_GV);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "autos";
            this.Text = "Автомобили";
            this.Load += new System.EventHandler(this.autos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.table_GV)).EndInit();
            this.auto_gb.ResumeLayout(false);
            this.auto_gb.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ComboBox cargo_tb;
        private System.Windows.Forms.ComboBox state_tb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button delete_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.DataGridView table_GV;
        private System.Windows.Forms.TextBox mark_tb;
        private System.Windows.Forms.TextBox number_g_tb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox capacity_tb;
        private System.Windows.Forms.GroupBox auto_gb;
    }
}
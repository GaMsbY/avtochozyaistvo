﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class clients : Form
    {
        MySqlConnection connection;
        ShowTables show;
        string ability = null;
        string request = "SELECT cl.id_client, cl.surname AS Фамилия, cl.address AS Адрес, cl.telephon AS Телефон FROM clients cl";
        public clients(MySqlConnection connection, string ability)
        {
            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false; ;

            if(ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                save_btn.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "clients");
            show.take_data(request);
            connection.Close();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            show.add_data_auto();
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            show.save_data();
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            show.delete_data_auto();
        }

        private void clients_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}

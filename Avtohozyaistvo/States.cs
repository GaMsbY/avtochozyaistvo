﻿using System;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Avtohozyaistvo
{
    public partial class States : Form
    {
        MySqlConnection connection;
        ShowTables show;
        string ability = null;
        string request = "SELECT st.id_state, st.state_a AS Состояние FROM states st";
        public States(MySqlConnection connection, string ability)
        {
            InitializeComponent();
            this.connection = connection;
            this.ability = ability;
            table_GV.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            table_GV.AllowUserToAddRows = false;
            table_GV.MultiSelect = false;

            if (ability == "read")
            {
                add_btn.Visible = false;
                delete_btn.Visible = false;
                save_btn.Visible = false;
                table_GV.Height = this.Height - 60;
            }

            connection.Open();
            show = new ShowTables(connection, table_GV, "states");
            show.take_data(request);
            connection.Close();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            show.add_data_auto();
        }

        private void save_btn_Click(object sender, EventArgs e)
        {
            show.save_data();
        }

        private void delete_btn_Click(object sender, EventArgs e)
        {
            show.delete_data_auto();
        }

        private void States_Load(object sender, EventArgs e)
        {
            connection.Open();
            show.update_database();
            connection.Close();
        }
    }
}
